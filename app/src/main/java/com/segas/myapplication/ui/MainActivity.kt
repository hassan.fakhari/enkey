package com.segas.myapplication.ui

import android.annotation.SuppressLint
import android.app.KeyguardManager
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.text.method.TextKeyListener.clear
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import co.tob.charan.constants.EncType
import co.tob.charan.constants.EncryptConstants
import co.tob.charan.encrytion.AesKeystoreWrapper
import co.tob.charan.encrytion.AesSaltEncryption
import co.tob.charan.encrytion.RsaKeystoreWrapper
import co.tob.charan.utils.*
import com.github.diver.data.api.ApiHelper
import com.segas.myapplication.R
import java.io.*
import java.security.KeyPair
import java.security.KeyStore
import java.security.SecureRandom
import java.util.concurrent.Executor
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.SecretKey
import javax.crypto.spec.SecretKeySpec


class MainActivity : AppCompatActivity() {

    val SHARED_PREFS_FILENAME = "biometric_prefs"
     val CIPHERTEXT_WRAPPER = "ciphertext_wrapper"
    private lateinit var executor: Executor
    private lateinit var biometricPrompt: BiometricPrompt
    private lateinit var promptInfo: BiometricPrompt.PromptInfo
lateinit var edt_txt : EditText
var key = "";
    private var isencrypt = true
    private lateinit var dataHashMap : HashMap<String, ByteArray>

    private var encType = EncType.AESENCRYPT

    private lateinit var rsaKeystoreWrapper : RsaKeystoreWrapper
    private lateinit var rsaKeyPair : KeyPair
    private var encrypteData : String = ""

    private lateinit var aesKeystoreWrapper: AesKeystoreWrapper

    private lateinit var aesSaltEncryption: AesSaltEncryption
    val model : MainViewModel = MainViewModel(ApiHelper, this)
   lateinit var ks: KeyStore
    var password = ""
      override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        edt_txt = findViewById(R.id.edt_txt)

          val keyguardManager = getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
          if(!isDeviceSecure(keyguardManager)) showDeviceSecurityAlert(this)

          if(Build.VERSION.SDK_INT >= 23) {

              aesKeystoreWrapper = AesKeystoreWrapper()
              aesSaltEncryption = AesSaltEncryption()

          }else{

              encType = EncType.RSAENCRYPT
          }

          rsaKeystoreWrapper = RsaKeystoreWrapper()
          EncType.AESENCRYPT


          isencrypt = true
          dataHashMap = HashMap()
          encrypteData = ""

          executor = ContextCompat.getMainExecutor(this)
        biometricPrompt = BiometricPrompt(this, executor,
            object : BiometricPrompt.AuthenticationCallback() {
                override fun onAuthenticationError(
                    errorCode: Int,
                    errString: CharSequence
                ) {
                    super.onAuthenticationError(errorCode, errString)
                    Toast.makeText(
                        applicationContext,
                        "Authentication error: $errString", Toast.LENGTH_SHORT
                    )
                        .show()
                }

                override fun onAuthenticationSucceeded(
                    result: BiometricPrompt.AuthenticationResult
                ) {
                    super.onAuthenticationSucceeded(result)


                    var current_key = edt_txt.text
GenerateKey(current_key.toString())

                }

                override fun onAuthenticationFailed() {
                    super.onAuthenticationFailed()
                    Toast.makeText(
                        applicationContext, "Authentication failed",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
            })

        promptInfo = BiometricPrompt.PromptInfo.Builder()
            .setTitle("Biometric login for my app")
            .setSubtitle("Log in using your biometric credential")
            .setNegativeButtonText("Use account password")
            .build()


        val biometricLoginButton =
            findViewById<Button>(R.id.biometric_login)
        biometricLoginButton.setOnClickListener {
            if(edt_txt.getText().length <=0){
                Toast.makeText(
                    applicationContext, "Password Is Empty",
                    Toast.LENGTH_SHORT
                )
                    .show()
            }else {

                biometricPrompt.authenticate(promptInfo)
            }
        }
        ks = KeyStore.getInstance(KeyStore.getDefaultType())



        var prf  = getSharedPreferences("keystore_prf", MODE_PRIVATE)

        var first_time = prf.getBoolean("first_time", false)

        if(first_time == false){
prf.edit().putBoolean("first_time", true).apply()
            model.getKeyStore()
            model.KeyStoreResponse.observe(this@MainActivity, Observer {

                this@MainActivity.runOnUiThread {
                    prf.edit().putString("MasterKey", it.char[0].toString()).apply()
                    password = it.char[0]
                    val response = encryptData(password)
                    if (response is HashMap<*, *>) {
                        dataHashMap = response as HashMap<String, ByteArray>

                        val ss: StringBuilder = StringBuilder()
                        ss.append(
                            "$encType EN-crypted Data \n\nIV - ${dataHashMap[EncryptConstants.IV_VALUE]!!.fromBytetoString()}" +
                                    "\nENC - ${dataHashMap[EncryptConstants.ENC_VALUE]!!.fromBytetoString()}"
                        )
                        if (encType == EncType.AESALTENCRYPT) {
                            ss.append("\nSLAT - ${dataHashMap[EncryptConstants.SALT_VALUE]!!.fromBytetoString()}")
                        }
                        Log.w("www", "onCreate: " + ss.toString())


                        val file = File(getDir("data", MODE_PRIVATE), "map")
                        val outputStream = ObjectOutputStream(FileOutputStream(file))
                        outputStream.writeObject(dataHashMap)
                        outputStream.flush()
                        outputStream.close()


                        /*
                        AESENCRYPT EN-crypted Data

    IV - unvYWZaLVgVwd2ho
    ENC - jOQpZIwkTaXZn/Z3wMUq1KEF53Q0Vx+CFhE6

                         */

                    } else if (response is String) {
                        encrypteData = response
                        Log.w("www", "onCreate: 12" + response)

                        prf.edit().putString("GenerateKey", encrypteData).apply()

                    }


                }

            })

        }else{

            Log.w("www", "onCreate: " + prf.getString("GenerateKey", ""))
try {
    val file = File(getDir("data", MODE_PRIVATE), "map")
    val inputStream = ObjectInputStream(FileInputStream(file))
    dataHashMap = inputStream.readObject() as HashMap<String, ByteArray>
    Log.w("www", "onCreate: " + dataHashMap[EncryptConstants.ENC_VALUE])

    inputStream.close()


    val data = if (encType == EncType.AESENCRYPT || encType == EncType.AESALTENCRYPT) {
        key = decryptData(dataHashMap)!!

    } else {
        var ds = decryptData(encrypteData)

    }
    isencrypt = true
}catch (e : Exception){

}



        }
    }




    var key1: SecretKey? = null





    fun GenerateKey(password: String) {
        try {

            key1 = KeyGenerator.getInstance("AES").generateKey()


            val kgen = KeyGenerator.getInstance("AES")
            val sr = SecureRandom.getInstance("SHA1PRNG")
            sr.setSeed(key.toByteArray())



            kgen.init(128, sr) // 192 and 256 bits may not be available

            val skey = kgen.generateKey()
            val raw = skey.encoded

            val skeySpec = SecretKeySpec(raw, "AES")
            val cipher: Cipher = Cipher.getInstance("AES")
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec)
            val encrypted: ByteArray = cipher.doFinal(password.toByteArray())

Toast.makeText(this@MainActivity,"Secret key created",Toast.LENGTH_LONG).show()









        } catch (ex: Exception) {
            Toast.makeText(this@MainActivity,ex.message.toString(),Toast.LENGTH_LONG).show()


        }
    }











    @SuppressLint("NewApi")
    private fun encryptData(data: String) : Any? {

        return when (encType) {
            EncType.AESENCRYPT -> {
                aesKeystoreWrapper.encryptData(data.toByteArray())
            }
            EncType.AESALTENCRYPT -> aesSaltEncryption.encrypt(data.toByteArray())
            else -> {

                if(hasMarshmallow()) {
                    rsaKeystoreWrapper.createAsymmetricKeyPair()
                    rsaKeyPair = rsaKeystoreWrapper.getAsymmetricKeyPair()!!
                }else{
                    rsaKeyPair = rsaKeystoreWrapper.createAsymmetricKeyPair()
                }
                rsaKeystoreWrapper.encrypt(data, rsaKeyPair.public)
            }
        }
    }





    @SuppressLint("NewApi")
    private fun decryptData(data: Any) : String? {
        return when (encType) {
            EncType.AESENCRYPT -> aesKeystoreWrapper.decryptData(data as HashMap<String, ByteArray>)
            EncType.AESALTENCRYPT -> aesSaltEncryption.decrypt(data as HashMap<String, ByteArray>)
            else -> rsaKeystoreWrapper.decrypt(data as String, rsaKeyPair.private)
        }
    }

    @Suppress("SENSELESS_COMPARISON")
    override fun onDestroy() {
        if(::rsaKeyPair.isInitialized)rsaKeystoreWrapper.removeKeyStoreKey()
        super.onDestroy()
    }




}