package com.segas.myapplication.ui

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.github.diver.data.api.APIService
import com.github.diver.data.api.ApiHelper
import com.segas.myapplication.di.model.responseKeyStore
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers

class MainViewModel(var api : ApiHelper,val context :Context) : ViewModel(){


    var KeyStoreResponse  = MutableLiveData<responseKeyStore>()
    var KeyStoreError = MutableLiveData<String>()

    fun getKeyStore(){
        val apiData = api.create().create(APIService :: class.java)
        apiData.getMainServer()?.observeOn(Schedulers.newThread())?.subscribeOn(AndroidSchedulers.mainThread())?.subscribe ({
            KeyStoreResponse.postValue(it)

        },{
            KeyStoreError.postValue(it.message)

        })
    }




}