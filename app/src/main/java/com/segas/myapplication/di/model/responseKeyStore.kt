package com.segas.myapplication.di.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class responseKeyStore(

    @Expose
    @SerializedName("char")
    var char : Array<String>

)