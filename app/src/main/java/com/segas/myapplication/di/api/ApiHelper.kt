package com.github.diver.data.api

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException

object ApiHelper {



    fun create() : Retrofit {
        val httpInseperator : HttpLoggingInterceptor = HttpLoggingInterceptor();
        httpInseperator.level = HttpLoggingInterceptor.Level.BODY
        val retrofit =
                Retrofit.Builder().addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(OkHttpClient().newBuilder().addInterceptor(httpInseperator).build())
                        .baseUrl("https://www.passwordrandom.com/").build()
        return retrofit

    }





     class AddHeaderInceptor : Interceptor {
        @Throws(IOException::class)
        override fun intercept(chain: Interceptor.Chain): Response {
            val builder = chain.request().newBuilder()
            builder.addHeader("Authorization", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MDI1NzUxMTEsInVzZXJfaWQiOjM4M30.VNS0Mbs5BaVlv32KIJXUCf6mwusbFy4enyO395tUXZM")
            return chain.proceed(builder.build())
        }
    }
}